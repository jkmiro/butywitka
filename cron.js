const CronJob = require('cron').CronJob;
const shell = require('shelljs');

const job = new CronJob('* */5 * * * *', function() {
  shell.exec('nightwatch tests/assertSize.js');
});

job.start();