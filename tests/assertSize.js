module.exports = {
  before: (browser, done) => {
    done();
  },

  after: (browser, done) => {
    console.log('Finishing');
    browser.end();
    done();
  },

  'Maximize window': browser => {
    browser.maximizeWindow();
  },

  'Go to Clothing category': browser => {
    browser.url('https://www.adidas.pl/ozweego-shoes/EE6465.html');
  },

  'Assert size present': async browser => {
    await browser.click('button[title="Wybierz rozmiar"]');
    browser.assert.elementPresent('button[title="38 2/3"]');
  }
};